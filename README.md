# DBnomics series to CSV

Extract DBnomics series data and produces normalized CSV files.

Data flow:

```
source_data/{ africa_countries_info.csv, dbnomics_series.yml, static-series } -> dist/csv_data -> dist/{ country_data, dr_data, pop_data }
source_data/{ indicators_info.csv } -> dist/indicators_info.json
```

## Source data

### DBnomics series

DBnomics series to process are defined in `source_data/dbnomics_series.yml` file.

Source: [tout_afrique_master README.md](https://nextcloud.jailbreak.paris/apps/files/?dir=/Jailbreak/Projets/AFD/Tout%20Afrique/tout_afrique-master&fileid=72416))

### African country codes

`source_data/africa_countries_info.csv` declare african country codes + _direction régionale_ info.

## Generated data

CSV data files:

- containing:
  - year as first column
  - 3-letter country codes as following columns
  - year values are sorted
- produced in `dist` folder

## Development

Create a virtual env and install requirements:

```bash
pip install -r requirements.txt
```

To download indicators from DBnomics, load other ones from `./source_data` dir, and produce data for Tout Afrique application in `dist`:

```bash
mkdir dist
./process_indicators.py dist
```

The `--only` option allows to process a subset of source data. For example this allows to skip the download of DBnomics data.

### Synchronize production data locally

```bash
pip install awscli awscli_plugin_endpoint

# export all variables of .env
set -a; source .env; set +a

aws s3 sync --no-sign-request s3://afd-toutafrique-data dist
```

## Production

Data can be updated on a regular basis via many options.

### Deploy to Git repo

It's also possible to setup a CI job that pushes generated data in a Git repository.

Then, on the production server:

- this repo is cloned in a directory
- in [Tout Afrique API](https://framagit.org/jailbreak/toutafrique/toutafrique-api), the environment variable `DATA_DIR` targets this directory
- a Cron job would do `git pull` once an hour

### Deploy to S3 bucket

Indicators processing is defined as a GitLab CI job in `.gitlab-ci.yml`. This job is set up to start each time a commit is pushed to this repository, and is [scheduled](https://framagit.org/jailbreak/toutafrique/toutafrique-data-processing/pipeline_schedules) once a day to benefit from DBnomics updates.

The next job named "Upload to S3" deploys generated data to the [production S3 bucket](https://console.scaleway.com/object-storage/buckets/fr-par/afd-toutafrique-data).

On the server, the S3 bucket is mounted as a directory using S3FS.

We recommend this option because no action is needed to update data.
