#!/usr/bin/env python3
"""
Generate color scale between white and given color.
"""
import argparse
import re
import sys

RGB_TRIPLET_RE = re.compile(r"^[0-9a-f]{6}$")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("scale_len", type=int, help="color scale length")
    parser.add_argument(
        "target_color", type=str, help="target color as hexadecimal triplet (00ff00)"
    )
    args = parser.parse_args()

    scale_len = args.scale_len
    if not RGB_TRIPLET_RE.match(args.target_color):
        parser.error("Invalid target_color: [%s]", args.target_color)
    target_color = args.target_color

    start_triplet = [255, 255, 255]
    target_triplet = [int(chunk, 16) for chunk in re.findall("..", target_color)]

    step_triplet = [
        (target - start) / scale_len
        for start, target in zip(start_triplet, target_triplet)
    ]

    color_list = []
    for i in range(scale_len):
        color_triplet = [
            (start + step * (i + 1)) for start, step in zip(start_triplet, step_triplet)
        ]
        rgb_color = "".join(["{:02x}".format(int(nb)) for nb in color_triplet])
        color_list.append(rgb_color)
    print(",".join(color_list))


if __name__ == "__main__":
    sys.exit(main())
